# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180107141333) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "channels", force: :cascade do |t|
    t.string "fb_id"
    t.string "ig_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "fb_url"
    t.index ["fb_id"], name: "index_channels_on_fb_id"
  end

  create_table "nodes", force: :cascade do |t|
    t.bigint "channel_id"
    t.string "node_id"
    t.boolean "is_video"
    t.integer "likes", default: 0
    t.integer "comments", default: 0
    t.datetime "date"
    t.string "thumbnail_src"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["channel_id", "date"], name: "index_nodes_on_channel_id_and_date"
    t.index ["channel_id"], name: "index_nodes_on_channel_id"
    t.index ["node_id"], name: "index_nodes_on_node_id"
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "channel_id"
    t.string "post_id"
    t.string "full_picture"
    t.string "source"
    t.string "message"
    t.datetime "created_time"
    t.integer "shares", default: 0
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["channel_id", "created_time"], name: "index_posts_on_channel_id_and_created_time"
    t.index ["channel_id"], name: "index_posts_on_channel_id"
    t.index ["post_id"], name: "index_posts_on_post_id"
  end

  add_foreign_key "nodes", "channels"
  add_foreign_key "posts", "channels"
end
