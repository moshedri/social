class CreateNodes < ActiveRecord::Migration[5.1]
  def change
    create_table :nodes do |t|
      t.references :channel, foreign_key: true
      t.string :node_id
      t.boolean :is_video
      t.integer :likes , default: 0
      t.integer :comments , default: 0
      t.datetime :date
      t.string :thumbnail_src

      t.timestamps
    end
    add_index :nodes, :node_id
    add_index :nodes, [:channel_id, :date]
  end
end
