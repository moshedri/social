class RenameFbIdInChannels < ActiveRecord::Migration[5.1]
  def change
 
    rename_column :channels, :ig_url, :ig_id
  end
end
