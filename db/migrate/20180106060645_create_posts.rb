class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.references :channel, foreign_key: true
      t.string :post_id
      t.string :full_picture
      t.string :source
      t.string :message
      t.datetime :created_time
      t.integer :shares , default: 0
      t.string :type

      t.timestamps
    end
    add_index :posts, :post_id
    add_index :posts, [:channel_id, :created_time]
  end
end

