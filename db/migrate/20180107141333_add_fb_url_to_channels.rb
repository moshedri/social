class AddFbUrlToChannels < ActiveRecord::Migration[5.1]
  def change
    add_column :channels, :fb_url, :string
  end
end
