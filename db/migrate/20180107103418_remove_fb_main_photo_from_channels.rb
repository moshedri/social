class RemoveFbMainPhotoFromChannels < ActiveRecord::Migration[5.1]
  def change
    remove_column :channels, :fb_main_photo, :string
    remove_column :channels, :ig_main_photo, :string
  end
end
