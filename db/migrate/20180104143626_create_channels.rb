class CreateChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :channels do |t|
      t.string :fb_id
      t.string :ig_url
      t.string :fb_main_photo
      t.string :ig_main_photo

      t.timestamps
    end
    add_index :channels, :fb_id
  end
end
