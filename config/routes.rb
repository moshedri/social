Rails.application.routes.draw do

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  root 'channel#home'
  post 'channel/scan' , to: 'channel#scan'
  get 'channel/:id' , to: 'channel#show'


end
