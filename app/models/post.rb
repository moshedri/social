class Post < ApplicationRecord
  belongs_to :channel

  def get_message
    Base64.decode64(self.message)
  end
end
