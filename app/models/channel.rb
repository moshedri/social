class Channel < ApplicationRecord
  has_many :posts
  has_many :nodes

  


      def ig
        @nodes = RubyInstagramScraper.get_user_media_nodes( self.ig_id )

        while !@nodes.nil? && @nodes.count >0

        @nodes.each do |node|
        if Time.at(node["date"]) < 30.days.ago
        return Node.where(channel_id: self.id).count   
        end


        @node = Node.find_or_initialize_by(node_id: node["id"])
        @node.is_video = node["is_video"]
        @node.channel_id = self.id 
        @node.likes = node["likes"]["count"]
        @node.comments = node["comments"]["count"]
        @node.date = Time.at(node["date"])
        @node.thumbnail_src= node["thumbnail_src"]
        @node.save!
          end

        sleep(0.3)
        @nodes = RubyInstagramScraper.get_user_media_nodes( self.ig_id  , @nodes.last["id"])
        end
     return Node.where(channel_id: self.id).count
      end

      def get_fb_id
begin
          @response = RestClient.get self.fb_url
         rescue RestClient::ExceptionWithResponse => e
 
         return 
         e.response
         end

         object = Nokogiri::HTML(@response)

         id = %w[description keywords].map { |name| object.at("meta[property='#{"al:android:url"}']")['content']}.first.gsub("fb://profile/" , "").gsub("fb://page/","").gsub("?referrer=app_link" , "")
      self.fb_id =id
      self.save!
        end

      def fb

       if self.fb_id.nil?
        self.get_fb_id
       end


        if self.fb_id.nil?
          return
        end



      @graph = Koala::Facebook::API.new("274707869645871|Ja1Tzx48its_MLcx2xKqAJvxapY")
      @posts =@graph.get_connection( self.fb_id  ,"posts",{fields: ["full_picture","type,source","message","created_time","object_id","shares"]})

  
     while  @posts != nil && @posts.count > 0

         @posts.each do |post|

          if post["created_time"] < 30.days.ago
            return Post.where(channel_id: self.id).count
            end
        @post = Post.find_or_initialize_by(post_id: post["object_id"])
        @post.channel_id = self.id 

        if !post["full_picture"].nil?
        @post.full_picture = post["full_picture"]
        end
        if !post["source"].nil?
        @post.source = post["source"]
        end
        if !post["message"].nil?
        @post.message = Base64.encode64(post["message"])
        end
        @post.created_time= post["created_time"]
        if post["shares"] != nil && post["shares"]["count"] != nil
        @post.shares = post["shares"]["count"].to_i
        end
                      
        @post.save!

          
          end

    @posts = @posts.next_page
      
      end

      end
end


