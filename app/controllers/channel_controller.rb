class ChannelController < ApplicationController
    def home
    end

    def scan 

    @ig_name = nil
    
     


        if params[:ig_url].present?
            @ig_name =  params[:ig_url].gsub("https://www.instagram.com/", "")
        end
        if @ig_name.nil? && params[:fb_url].present?

            render  json: {code: 404 , error: " can't save empty channel " }

        else
       @channel =  Channel.create!(fb_url: params[:fb_url] , ig_id: @ig_name)
       if @channel.ig_id != nil
        Ig_Worker.perform_async(@channel.id)
       end

       if @channel.fb_url != nil
       Fb_Worker.perform_async(@channel.id)
       end
          render :json =>  @channel.as_json
      
        end
    end

    def show 
       
        if Channel.where(id: params[:id]).exists?

         @channel = Channel.find(params[:id])
   
 
       render :json =>  {  channel: @channel.as_json , posts: @channel.posts.as_json , nodes:@channel.nodes.as_json}



        else
        
            
            render  json: {code: 404 , error: " could not find channel with id "+ params[:id].to_s}
            

        end


    end

   
    def as_json(options={})
    super(
          :include => {
            :posts => {:only => [:post_id]},
            :nodes => {:only => [:node_id]}
          }
    )
  end   

end
